FROM adoptopenjdk/openjdk11:alpine

ENV MYSQL_USER="operations"
ENV MYSQL_PASSWORD="5TTnvuTDJJSq6"

RUN addgroup -S spring && adduser -S spring -G spring

USER spring:spring

VOLUME /tmp

ARG JAR_FILE

ADD ${JAR_FILE} /app/app.jar

EXPOSE 8080

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/app.jar"]
